#!/bin/sh
set -eu

bail() {
    printf "%s\n" "$1" >&2
    exit 1
}

usage() {
    echo "Usage: <tdx-command>" >&2
    echo "Available commands:"
    find . -name "tdx-*" -type f -perm /0111 | sed "s/^\.\/tdx-/    /" >&2
    exit ${1:-0}
}

while getopts h opt; do
    case $opt in
    h) usage ;;
    *) usage 1 ;;
    esac
done
shift $((OPTIND - 1))

[ $# -lt 1 ] && usage 1
cmd=$1
shift

printf "%s" "$cmd" | grep -q "^[a-z][a-z-]*[a-z]$" || bail "No."
[ ! -f "tdx-$cmd" ] || [ ! -x "tdx-$cmd" ] && bail "No such subcommand: $cmd."

[ -z "${BEID-}" ] && bail "BEID must be set."
[ -z "${WebServicesKey-}" ] && bail "WebServicesKey must be set."

sed 's/^ \{4\}//' <<EOF >config.yml
    tdx_connection:
        BEID: $BEID
        WebServicesKey: $WebServicesKey
EOF

cd /tmp

exec "$OLDPWD/tdx-$cmd" "$@"
