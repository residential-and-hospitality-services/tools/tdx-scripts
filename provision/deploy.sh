set -e
shopt -s nullglob

virtualenv --quiet env
source env/bin/activate
pip install --quiet /srv/tdx-scripts/wheels/*.whl

[[ -s ~/.bash_profile ]] && echo >>~/.bash_profile
sed 's/ \{4\}//' <<'END' >>~/.bash_profile
    if [[ $- == *i* ]]; then
        if [[ -z $VIRTUAL_ENV ]]; then
            echo "Activating virtualenv at ~/env..."
            source ~/env/bin/activate
        fi

        cd /srv/tdx-scripts
    fi
END
