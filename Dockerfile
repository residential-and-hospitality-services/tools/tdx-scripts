FROM python:2.7-alpine

WORKDIR /usr/src/app

RUN touch config.yml \
    && chown nobody: config.yml

COPY entrypoint /
COPY wheels/ wheels/
COPY requirements.txt tdx-* ./

RUN find . -type d -exec chmod 0755 \{\} \; \
    && find . -type f -exec chmod 0644 \{\} \; \
    && chmod 0755 /entrypoint tdx-*

RUN pip install --quiet wheels/*.whl

USER nobody
ENTRYPOINT ["/entrypoint"]
