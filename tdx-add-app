#!/usr/bin/env python

import codecs
import getopt
import locale
from os.path import abspath, dirname, join
import sys
import yaml

import tdapi
from tdapi.person import TDPerson, TDPersonManager

# Wrap sys.stdout into a StreamWriter to allow writing UTF-8 characters.
# Source: <https://stackoverflow.com/a/4546129>.
sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout)

def usage(status=0):
    print "Usage: tdx-add-app <appname>"
    sys.exit(status)

def describe_person(person):
    return "#%s %s (%s)" % (person["UID"], person["FullName"],
            person["UserName"])

def main(argv):
    dry_run = False

    with open(join(abspath(dirname(__file__)), "config.yml"), "r") as ymlfile:
        cfg = yaml.load(ymlfile)

    try:
        opts, args = getopt.getopt(argv, "nh")
    except getopt.GetoptError:
        usage(1)

    for opt, arg in opts:
        if opt == "-n":
            dry_run = True
        if opt == "-h":
            usage()

    if len(args) != 1:
        usage(1)

    app_to_add = args[0]

    print "Adding application %s to all users%s..." % \
            (app_to_add, " (dry-run)" if dry_run else "")

    tdapi.set_connection(
        tdapi.TDConnection(
            BEID = cfg["tdx_connection"]["BEID"],
            WebServicesKey = cfg["tdx_connection"]["WebServicesKey"]))

    manager = TDPersonManager()

    for person in manager.search({"IsActive": True, "MaxResults": 100000}):
        person = manager.get(person["UID"])

        if app_to_add not in person["Applications"]:
            print "ADD " + describe_person(person)
            if not dry_run:
                person.add_applications([app_to_add])
        else:
            print "ALREADY " + describe_person(person)

if __name__ == "__main__":
    main(sys.argv[1:])
