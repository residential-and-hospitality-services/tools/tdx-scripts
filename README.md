# tdx-scripts: Tools for performing various bulk actions on TeamDynamix accounts

* `tdx-add-app`: Adds an application (e.g., TDRequests) to every TeamDynamix
  account.

* `tdx-change-email-addresses`: Performs a regex search and replace against each
  account's primary and alert email addresses.

* `tdx-list-users`: List each user's UID, full name, and primary email address.

## Usage

### tdx-add-app

    td-add-app [-n] <application>

For each processed account, it prints a line in the form:

    (ADD | ALREADY) #<uid> <name> \(<email>\)

With `ADD` when the application is to be added to the account or `ALREADY` when
it is already.

If called with `-n`, it executes a dry run.

### tdx-change-email-addresses

    tdx-change-email-addresses [-n] <pattern> <replacement>

If called with `-n`, it executes a dry run.

## Installation

The included Vagrantfile will provision a working environment for these tools.

They can also be run with Docker. Pass in the configuration paramaters as
environment variables:

```sh
sudo docker build -t tdx-scripts .
sudo docker run --rm tdx-scripts:latest -e BEID=x -e WebServicesKey=y list-users
```

### Manual installation

#### Requirements

* [Python 2](<https://www.python.org/downloads/>)
* [virtualenv](<https://virtualenv.pypa.io>)

#### Setup

1. Create a Python virtual environment:

    ```sh
    virtualenv env
    ```

2. Switch into that environment:

    ```sh
    source env/bin/activate
    ```

3. Install project requirements:

    ```sh
    pip install wheels/*.whl
    ```

## Developing

If you change any dependencies, you ought to update and commit the
`requirements.txt` and the package cache. Inside the virtualenv, execute:

```sh
pip freeze >requirements.txt
pip wheel --wheel-dir=wheels -r requirements.txt
```
